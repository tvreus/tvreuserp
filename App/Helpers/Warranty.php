<?php
namespace App\Helpers;

use app\Models\WarrantyModel;

class Warranty
{
    public static function makeOrderWarranty($order_id,  $date)
    {
        $order_warranty=WarrantyModel::makeWarranty($order_id,$date);
        return $order_warranty;
    }

    public static function makeProductWarranty($order_id,$date)
    {
        $products=WarrantyModel::getWarrantyProductsperOrder($order_id);
        foreach($products as $product)
        {
            $product_id=$product['product_id'];
            $warranty=WarrantyModel::makeProductWarranty($order_id,$date,$product_id);
            return $warranty;
        }
    }


}