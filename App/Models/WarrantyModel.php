<?php


namespace app\Models;
use PDO;
use Core\Model;


class WarrantyModel extends Model
{
    public static function makeWarranty($order_id,$date)

    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('INSERT INTO order_warranty 
            order_id=?, order_date=?
              ');
             $stmt->execute([$order_id,$date]);
             $stmt=null;
            $warranty_id = $db->lastInsertId();
            return $warranty_id;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function makeProductWarranty($order_id,$date,$product_id)

    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('INSERT INTO order_warranty 
            order_id=?, order_date=?,$product_id
              ');
            $stmt->execute([$order_id,$date,$product_id]);
            $stmt=null;
            $warranty_id = $db->lastInsertId();
            return $warranty_id;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getWarrantyProductsperOrder($order_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM order_warranty 
            WHERE order_id=?  ');
            $results = $stmt->execute([$order_id,$date,$product_id]);
            $results = $stmt->fetch();
            return $results;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }
}