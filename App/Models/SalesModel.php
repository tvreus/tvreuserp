<?php


namespace app\Models;
use PDO;
use Core\Model;


class SalesModel extends Model
{
    public static function getOrders()
    {
        try {
            $db = static::getDB();
            $stmt = $db->query('SELECT * FROM orders AS r 
            
            JOIN order_status AS x ON r.order_id=x.order_id
            JOIN order_status_types AS y ON x.order_status =y.status_id
            JOIN order_products AS p on p.order_id=r.order_id
            ORDER BY order_date ');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function setStatus($order_status, $order_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('UPDATE orders SET 
            order_status=?  WHERE order_id=? ');
            $results = $stmt->execute([$order_id]);

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getOrder($order_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM orders AS r 
            
            JOIN order_status AS x ON r.order_id=x.order_id
            JOIN order_status_types AS y ON x.order_status =y.status_id
            JOIN order_products AS p on p.order_id=r.order_id
             WHERE order_id=? ');
            $results = $stmt->execute([$order_id]);

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getDailySales($date)

     {
         try {
             $db = static::getDB();
             $stmt = $db->prepare('SELECT * FROM orders AS r 
            
            JOIN order_status AS x ON r.order_id=x.order_id
            JOIN order_status_types AS y ON x.order_status =y.status_id
            JOIN order_products AS p on p.order_id=r.order_id
             WHERE or.date=? ');
             $results = $stmt->execute([$date]);

         } catch (\PDOException $e) {
             echo $e->getMessage();
         }
     }


}