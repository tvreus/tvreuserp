<?php

/**
 * Created by PhpStorm.
 * User: simona
 * Date: 27/09/2018
 * Time: 14:06
 */

namespace App;
class Config
{
    const DB_HOST = 'localhost';
    const DB_NAME = 'tvreus';
    const DB_USER = 'root';
    const DB_PASSWORD = '';
    const SHOW_ERRORS = true;


    /*remote setup
        const DB_HOST='localhost';
        const DB_NAME='';
        const DB_USER='y';
        const DB_PASSWORD='';
        const SHOW_ERRORS=true;
    */
}
