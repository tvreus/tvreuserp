<?php


namespace app\Controllers;
use Core\Controller;
use \Core\View;
require '..\vendor\autoload.php';

class Personnel extends Controller
{
    public function IndexAction()
    {

     View::renderTemplate('Personnel/index.html');

    }

    public function StaffAction()
    {

        View::renderTemplate('Personnel/staff.html');

    }

}