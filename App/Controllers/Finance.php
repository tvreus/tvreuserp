<?php


namespace app\Controllers;
use Core\Controller;
use \Core\View;
use app\Models\SalesModel;
require '..\vendor\autoload.php';

class Finance extends Controller
{
    public function IndexAction()
    {

        View::renderTemplate('Finance/index.html');

    }


    public function OrdersAction()
    {
        $orders=SalesModel::getOrders();
        View::renderTemplate('Finance/orders.html',[
            'orders'=>$orders
        ]);

    }

    public function SalesreportsAction()
    {
        $orders=SalesModel::getOrders();
        View::renderTemplate('Finance/salesreports.html',[
            'orders'=>$orders
        ]);

    }


}