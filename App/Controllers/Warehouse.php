<?php


namespace app\Controllers;
use Core\Controller;
use \Core\View;
require '..\vendor\autoload.php';

class Warehouse extends Controller
{
    public function IndexAction()
    {

        View::renderTemplate('Warehouse/index.html');

    }
}