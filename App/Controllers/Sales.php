<?php



namespace app\Controllers;
use Core\Controller;
use \Core\View;
require '..\vendor\autoload.php';

class Sales extends Controller
{
    public function IndexAction()
    {

        View::renderTemplate('Sales/index.html');

    }

    public function TestAction()
    {

        View::renderTemplate('Sales/test.html');

    }
}