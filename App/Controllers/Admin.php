<?php


namespace app\Controllers;
use Core\Controller;
use \Core\View;
require '..\vendor\autoload.php';

class Admin extends Controller
{
    public function IndexAction()
    {

        View::renderTemplate('Admin/index.html');

    }
}