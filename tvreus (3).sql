-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 26, 2019 at 02:57 PM
-- Server version: 5.7.26
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tvreus`
--

-- --------------------------------------------------------

--
-- Table structure for table `company_identities`
--

DROP TABLE IF EXISTS `company_identities`;
CREATE TABLE IF NOT EXISTS `company_identities` (
  `company_id` int(15) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) NOT NULL,
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_identities`
--

INSERT INTO `company_identities` (`company_id`, `company_name`) VALUES
(1, 'TVReus'),
(2, 'Megatronic');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(15) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `customer_email` varchar(50) NOT NULL,
  `customer_password` varchar(15) NOT NULL,
  `customer_address_id` int(15) NOT NULL,
  `customer_payment_type` int(15) NOT NULL,
  `order_company` int(15) NOT NULL,
  `customer_type` int(15) NOT NULL,
  `mail_list_subscribe` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_address`
--

DROP TABLE IF EXISTS `customer_address`;
CREATE TABLE IF NOT EXISTS `customer_address` (
  `address_id` int(15) NOT NULL,
  `customer_id` int(15) NOT NULL,
  `street no` int(15) NOT NULL,
  `addition` varchar(10) NOT NULL,
  `street name` varchar(50) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `department_id` int(15) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(50) NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `department_name`) VALUES
(1, 'Sales'),
(2, 'IT'),
(3, 'Warehouse'),
(4, 'Logistics'),
(5, 'Finance'),
(6, 'Management');

-- --------------------------------------------------------

--
-- Table structure for table `house_offer_query`
--

DROP TABLE IF EXISTS `house_offer_query`;
CREATE TABLE IF NOT EXISTS `house_offer_query` (
  `query_id` int(15) NOT NULL,
  `customer_id` int(15) NOT NULL,
  `property_id` int(15) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `housing_customers`
--

DROP TABLE IF EXISTS `housing_customers`;
CREATE TABLE IF NOT EXISTS `housing_customers` (
  `customer_id` int(15) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `customer_email` varchar(50) NOT NULL,
  `customer_phone` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `company` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `order_total` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date_shipped` int(11) NOT NULL,
  `order_status` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_warranty`
--

DROP TABLE IF EXISTS `order_warranty`;
CREATE TABLE IF NOT EXISTS `order_warranty` (
  `warranty_id` int(15) NOT NULL,
  `order_id` int(15) NOT NULL,
  `order_date` date NOT NULL,
  `warranty_end_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `payment_id` int(15) NOT NULL,
  `customer_id` int(15) NOT NULL,
  `payment_type_id` int(25) NOT NULL,
  `total` float NOT NULL,
  `tax` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

DROP TABLE IF EXISTS `payment_types`;
CREATE TABLE IF NOT EXISTS `payment_types` (
  `payment_type_id` int(15) NOT NULL,
  `payment_name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
CREATE TABLE IF NOT EXISTS `product_category` (
  `category_id` int(15) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`category_id`, `category_name`) VALUES
(1, 'LED TV'),
(2, 'Ultra HD TV'),
(3, 'Curved TV'),
(4, 'Soundbar'),
(5, 'Home Cinema'),
(6, 'Wall Brackets'),
(7, 'BluRay'),
(8, 'DVD Player'),
(9, 'Accessories'),
(10, 'Coffee Makers'),
(11, 'Beamers'),
(12, 'Hand Mixers'),
(13, 'Refrigerators'),
(14, 'Washing machines'),
(15, 'Dryers'),
(16, 'Cookers'),
(17, 'Vacuum cleaners'),
(18, 'Microwaves');

-- --------------------------------------------------------

--
-- Table structure for table `product_sku`
--

DROP TABLE IF EXISTS `product_sku`;
CREATE TABLE IF NOT EXISTS `product_sku` (
  `sku_id` int(15) NOT NULL,
  `sku` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

DROP TABLE IF EXISTS `product_types`;
CREATE TABLE IF NOT EXISTS `product_types` (
  `product_type_id` int(15) NOT NULL,
  `type_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_warranty`
--

DROP TABLE IF EXISTS `product_warranty`;
CREATE TABLE IF NOT EXISTS `product_warranty` (
  `product_warranty_id` int(15) NOT NULL,
  `warranty_id` int(15) NOT NULL,
  `order_id` int(15) NOT NULL,
  `product_id` int(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_offer`
--

DROP TABLE IF EXISTS `property_offer`;
CREATE TABLE IF NOT EXISTS `property_offer` (
  `property_id` int(15) NOT NULL,
  `street_no` int(4) NOT NULL,
  `street_name` varchar(50) NOT NULL,
  `postcode` varchar(6) NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `property_type` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property_offer_pics`
--

DROP TABLE IF EXISTS `property_offer_pics`;
CREATE TABLE IF NOT EXISTS `property_offer_pics` (
  `pic_id` int(15) NOT NULL,
  `property_offer_id` int(15) NOT NULL,
  `image_name` varchar(50) NOT NULL,
  `image_location` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(15) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'Director'),
(2, 'Manager'),
(3, 'CS representative'),
(4, 'Warehouse worker');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `user_id` int(15) NOT NULL,
  `date` date NOT NULL,
  `task_title` varchar(255) NOT NULL,
  `task_body` varchar(255) NOT NULL,
  `assigned_by` int(15) NOT NULL,
  `task_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tv_size`
--

DROP TABLE IF EXISTS `tv_size`;
CREATE TABLE IF NOT EXISTS `tv_size` (
  `size_id` int(15) NOT NULL,
  `size` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(12) NOT NULL,
  `user_department` int(15) NOT NULL,
  `user_role` int(15) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_role` (`user_role`),
  KEY `user_department` (`user_department`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_password`, `user_department`, `user_role`) VALUES
(1, 'Taron', 't.haik@tvreus.nl', 'tvreus1989', 6, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `dep` FOREIGN KEY (`user_department`) REFERENCES `departments` (`department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `rol` FOREIGN KEY (`user_role`) REFERENCES `roles` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
